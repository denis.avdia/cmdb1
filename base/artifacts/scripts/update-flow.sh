#!/bin/bash
APP_PATH=/var/www/html/itop
TEMP_PATH=/tmp/itop
if [[ -d ${APP_PATH}/datamodels ]]; then
    rm -rf ${TEMP_PATH}
    git clone https://gitlab.com/denis.avdia/cmdb1-flow.git ${TEMP_PATH}
    cp -r ${TEMP_PATH}/datamodels/* ${APP_PATH}/datamodels
  
    EXTENSIONS=${APP_PATH}/extensions/*
    for DST in ${EXTENSIONS}
    do
        SRC=${TEMP_PATH}/extensions/${DST##*/}
    if [[ -d ${SRC} ]]; then
            cp -rf ${SRC}/* ${DST}
    fi
    done
    /make-itop-config-writable.sh
# Setup changes 	
	php unattended_install.php  --response_file=default-params.xml
	
    rm -rf ${TEMP_PATH}
else
    echo "Directories $APP_PATH/datamodels not found"
fi
