#!/bin/bash
APP_PATH=/var/www/html/itop
TEMP_PATH=/tmp/itop
PHP=/usr/bin/php
Password=$(cat "/tmp/password"|base64 --decode)
Sync=/var/www/itop/web/synchro
csv=/var/www/itop/web/extensions/ldap-data-collector/data

if [[ -d ${APP_PATH}/extensions ]]; then
    
    EXTENSIONS=${APP_PATH}/extensions/*
    for DST in ${EXTENSIONS}
    do
        SRC=${TEMP_PATH}/extensions/${DST##*/}
    if [[ -d ${SRC} ]]; then
            cp -rf ${SRC}/* ${DST}
    fi
    done
    /make-itop-config-writable.sh
# collect users from ldap-HO 	
	$PHP ldap-data-collector/exec.php --console_log_level=9 --collect_only
# Import users collected
$PHP $Sync/synchro_import.php --auth_user=admin --auth_pwd="$Password" --csvfile=$csv/iTopUserLDAPCollector-1.csv --output=summary --data_source_id=8
# Import users login 
$PHP $Sync/synchro_import.php --auth_user=admin --auth_pwd="$Password" --csvfile=$csv/iTopPersonLDAPCollector-1.csv --output=summary --data_source_id=9
    rm -rf ${TEMP_PATH}
else
    echo "Directories $APP_PATH/datamodels not found"
fi




