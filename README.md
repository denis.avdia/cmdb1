# cmdb1

Usage
Run new iTop 2.7.2-1 (see tags for other iTop versions) container named my-itop:

sudo docker run -d -p 8000:80 --name=my-itop vbkunin/2.7.2-1
Then go to http://cmdb.unionx.al:8000/ to continue the installation.

Or the automatic setup is in the script: "update-flow.sh"
"php unattended_install.php  --response_file=default-params.xml"
Use this command to get the MySQL user credentials:

sudo docker logs my-itop | grep -A7 -B1 "Your MySQL user 'admin' has password:"
or use username root with blank password.

Expose iTop extensions folder if you need it:

sudo docker run -d -p 8000:80 --name=my-itop -v /home/user/itop-extensions:/var/www/html/extensions vbkunin/itop:2.7.2-1

Image without MySQL
Starting from 2.6.0 you can get base image without MySQL database server (only Apache and PHP) to use with your own one:
sudo docker run -d -p 8000:80 --name=my-itop vbkunin/itop:2.7.2-1
Useful scripts and helpers
The image ships with several useful scripts you can run like this:
sudo docker exec my-itop /script-name.sh [script_params]
If you need the iTop Toolkit you can simply get this:

sudo docker exec my-itop /install-toolkit.sh

A cron setup helper is aboard:

sudo docker exec my-itop /setup-itop-cron.sh Cron Pa$5w0rD

Then you should create iTop user account with login Cron and password Pa$5w0rD and grant him Administrator profile. The third argument (optional) is the absolute path to the log file or --without-logs key. By default, the log file is /var/log/itop-cron.log.

There are other scripts:
make-itop-config-writable.sh (or you can use conf-w shortcut without the leading slash: docker exec my-itop conf-w)
make-itop-config-read-only.sh (or conf-ro shortcut: docker exec my-itop conf-ro)
update-flow.sh - pull and install latest version from https://github.com/denis.avdia/cmdb1
Developer's corner

If you're using this image for development (especially with PhpStorm), there are a few things for you.
install-xdebug.sh – install Xdebug PHP extension and setup it for remote debugging. Two arguments are xdebug.client_port and xdebug.idekey (defaults are 9003 and PHPSTORM, respectively).
sudo docker exec my-itop /install-xdebug.sh [client_port] [idekey]
start-itop-cron-debug.sh – start remote debugging of iTop background tasks script (cron.php). The first two arguments are iTop user and his password (admin and password) and the third argument is debug server configuration name (default is localhost) in PhpStorm which specified through PHP_IDE_CONFIG environment variable (more details).
sudo docker exec my-itop /start-itop-cron-debug.sh [auth_user] [auth_pwd] [php_ide_server_name]
enable-mysql-remote-connections.sh – add the bind-address = 0.0.0.0 directive to the MySQL configuration to allow connections from outside the container.
sudo docker exec my-itop /enable-mysql-remote-connections.sh
Do not forget to expose the MySQL port with -p 3306:3306 when running the container.
